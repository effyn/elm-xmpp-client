var client;
var config;

var app = Elm.Main.init({
  node: document.getElementById("root"),
  flags: {
    connectJid: localStorage.getItem("jid"),
    connectPass: localStorage.getItem("pass"),
    wsUrl: localStorage.getItem("wsUrl"),
  }
});

var storedAvatars;

function portAvatar(jid, data) {
  app.ports.xmppMsg.send({
    tag: "SetAvatar",
    from: jid,
    data: data,
  });
}

function portNewAvatar(jid, type, data) {
  data = "data:" + (type || "image/png") + ";base64," + data;
  portAvatar(jid, data);
  storedAvatars[jid] = data;
}

function portPresence(jid, presence, status) {
  app.ports.xmppMsg.send({
    tag: "Presence",
    jid: jid,
    show: presence,
    status: status,
  });
}

app.ports.xmppCmd.subscribe(function(cmd) {
  if (cmd.tag === "Connect") {
    config = {
      wsUrl: cmd.wsUrl,
    };
    connect(cmd.jid, cmd.password);
  }

  else if (cmd.tag === "SendMessage") {
    client.sendMessage({
      to: cmd.to,
      body: cmd.body,
    });
  }

  else if (cmd.tag === "AddContact") {
    client.subscribe(cmd.jid);
  }

  else if (cmd.tag === "RemoveContact") {
    client.unsubscribe(cmd.jid);
  }
});

function connect(jid, password) {
  client = XMPP.createClient({
    jid: jid,
    password: password,
    transport: "websocket",
    wsURL: config.wsUrl,
  });

  client.on("session:started", function() {
    storedAvatars = {};
    client.enableCarbons();

    app.ports.xmppMsg.send({
      tag: "Connected",
      jid: client.jid,
    });

    client.getRoster(function(err, rep) {
      app.ports.xmppMsg.send({
        tag: "Roster",
        items: rep.roster.items,
      });
    });
    client.sendPresence();
  });

  client.on("chat", function(msg) {
    app.ports.xmppMsg.send({
      tag: "MessageReceived",
      from: msg.from,
      body: msg.body,
    });
  });

  client.on("avatar", function(evt) {
    let jid = evt.jid.bare;

    if (storedAvatars.hasOwnProperty(jid)) {
      return portAvatar(jid, storedAvatars[jid]);
    }

    if (evt.source === "pubsub") {
      client.getAvatar(jid, evt.avatars[0].id, function(err, avatar) {
        if (!err) {
          portNewAvatar(jid, evt.avatars[0].type, avatar.pubsub.retrieve.item.avatarData);
        }
      });
    }

    else if (evt.source === "vcard") {
      client.getVCard(jid, function(err, resp) {
        if (!err) {
          portNewAvatar(jid, resp.vCardTemp.photo.type, resp.vCardTemp.photo.data);
        }
      });
    }
  });

  client.on("presence", function(pres) {
    if (pres.type === "available") {
      portPresence(pres.from, pres.show || pres.type, pres.status);
    }

    else if (pres.type === "unavailable") {
      portPresence(pres.from, pres.type, pres.status);
    }
  });

  client.connect();
}
