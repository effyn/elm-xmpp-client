Dependencies
------------

Grab stanzaio.browser.js from [here](https://github.com/legastero/stanza.io). This needs to be in the repo root.

Running
-------

Compile the elm code to a file named `main.js` in the root and open index.html in a browser.

Make sure to set these localStorage keys:
  - `jid` to the jid to connect to
  - `pass` to the password
  - `wsUrl` to the WebSocket URL
