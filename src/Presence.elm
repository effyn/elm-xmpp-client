module Presence exposing (Presence, Show(..), parseShow, unavailable)


type alias Presence =
    { show : Show
    , status : Maybe String
    }


type Show
    = Available
    | Chat
    | Away
    | ExtendedAway
    | Busy
    | Unavailable


unavailable : Presence
unavailable =
    { show = Unavailable
    , status = Nothing
    }


parseShow : String -> Show
parseShow txt =
    case txt of
        "available" ->
            Available

        "chat" ->
            Chat

        "away" ->
            Away

        "xa" ->
            ExtendedAway

        "dnd" ->
            Busy

        "unavailable" ->
            Unavailable

        _ ->
            Unavailable
