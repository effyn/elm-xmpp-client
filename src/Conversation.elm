module Conversation exposing (Conversation, Message, addMessage, empty, setInput)

import Jid exposing (Jid)


type alias Conversation =
    { jid : Jid
    , messages : List Message
    , textInput : String
    }


type alias Message =
    { from : Jid
    , body : String
    }


empty : Jid -> Conversation
empty jid =
    { jid = jid, messages = [], textInput = "" }


addMessage : Message -> Conversation -> Conversation
addMessage msg conv =
    { conv | messages = conv.messages ++ [ msg ] }


setInput : String -> Conversation -> Conversation
setInput s conv =
    { conv | textInput = s }
