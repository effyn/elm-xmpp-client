module Main exposing (main)

import Browser
import Color
import Contact exposing (Contact)
import Conversation exposing (Conversation)
import Dict exposing (Dict)
import Html as H exposing (Html)
import Html.Attributes as HA
import Html.Events as HE
import Jid exposing (Jid)
import Json.Decode as JD
import Palette.Tango
import Presence
import Xmpp


main =
    Browser.element
        { init = init
        , subscriptions = subscriptions
        , update = update
        , view = view
        }


type alias MyInfo =
    { jid : Jid
    }


type alias Model =
    { myInfo : Maybe MyInfo
    , conversations : Dict String Conversation
    , contacts : List Contact
    , rosterTab : RosterTab
    , page : Page
    }


type Page
    = ConversationPage Jid
    | EmptyPage


type RosterTab
    = ContactsTab
    | ConversationsTab


type Msg
    = XmppMsg Xmpp.Msg
    | SetPage Page
    | SetRosterTab RosterTab
    | SetConversationInput String
    | SendMessage Jid String


onEnter : msg -> H.Attribute msg
onEnter msg =
    let
        checkKeyCode kc =
            if kc == 13 then
                JD.succeed True

            else
                JD.fail "wrong keycode"
    in
    HE.preventDefaultOn "keydown"
        (HE.keyCode
            |> JD.andThen checkKeyCode
            |> JD.map (\pd -> ( msg, pd ))
        )


makeXmppMsg : Result JD.Error Xmpp.Msg -> Msg
makeXmppMsg r =
    case r of
        Ok m ->
            XmppMsg m

        Err e ->
            Debug.todo (JD.errorToString e)


init : { wsUrl : String, connectJid : String, connectPass : String } -> ( Model, Cmd msg )
init flags =
    let
        model =
            { myInfo = Nothing
            , conversations = Dict.empty
            , page = EmptyPage
            , contacts = []
            , rosterTab = ContactsTab
            }

        cmd =
            Xmpp.cmd (Xmpp.Connect flags.wsUrl (Jid.parse flags.connectJid) flags.connectPass)
    in
    ( model, cmd )


subscriptions : Model -> Sub Msg
subscriptions model =
    Xmpp.sub makeXmppMsg


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        XmppMsg xm ->
            handleXmppMsg xm model

        SendMessage jid body ->
            case model.myInfo of
                Just myInfo ->
                    ( model |> setConversationInput "" |> addMessage jid { from = myInfo.jid, body = body }, Xmpp.cmd (Xmpp.SendMessage jid body) )

                Nothing ->
                    ( model, Cmd.none )

        SetPage p ->
            ( { model | page = p }, Cmd.none )

        SetConversationInput s ->
            ( setConversationInput s model, Cmd.none )

        SetRosterTab t ->
            ( { model | rosterTab = t }, Cmd.none )


handleXmppMsg : Xmpp.Msg -> Model -> ( Model, Cmd Msg )
handleXmppMsg xm model =
    case xm of
        Xmpp.Connected jid ->
            let
                info =
                    { jid = jid }
            in
            ( { model | myInfo = Just info }, Cmd.none )

        Xmpp.MessageReceived jid body ->
            ( model |> addMessage jid { from = jid, body = body }, Cmd.none )

        Xmpp.Roster roster ->
            ( { model | contacts = roster }, Cmd.none )

        Xmpp.ContactPresence jid pres ->
            ( { model
                | contacts =
                    model.contacts
                        |> List.map
                            (\c ->
                                if Jid.bare c.jid == Jid.bare jid then
                                    Contact.setPresence pres c

                                else
                                    c
                            )
              }
            , Cmd.none
            )

        Xmpp.SetAvatar jid avatar ->
            ( { model
                | contacts =
                    model.contacts
                        |> List.map
                            (\c ->
                                if Jid.bare c.jid == Jid.bare jid then
                                    Contact.setAvatar avatar c

                                else
                                    c
                            )
              }
            , Cmd.none
            )

        _ ->
            ( model, Cmd.none )


upsertConversation : (Conversation -> Conversation) -> Jid -> Model -> Model
upsertConversation f jid model =
    { model
        | conversations =
            Dict.update (Jid.toString jid)
                (\m ->
                    m |> Maybe.withDefault (Conversation.empty jid) |> f |> Just
                )
                model.conversations
    }


withCurrentConversation : (Conversation -> Conversation) -> Model -> Model
withCurrentConversation f model =
    case model.page of
        ConversationPage jid ->
            upsertConversation f jid model

        _ ->
            model


setConversationInput : String -> Model -> Model
setConversationInput s =
    withCurrentConversation (\conv -> conv |> Conversation.setInput s)


addMessage : Jid -> Conversation.Message -> Model -> Model
addMessage conversationJid msg model =
    let
        messageUpdate m =
            m |> Maybe.withDefault (Conversation.empty conversationJid) |> Conversation.addMessage msg |> Just
    in
    { model | conversations = Dict.update (Jid.toString conversationJid) messageUpdate model.conversations }


viewMessage : Conversation.Message -> Html Msg
viewMessage msg =
    H.div [ HA.class "message" ]
        [ H.div [ HA.class "message-jid" ] [ H.text (Jid.toString msg.from) ]
        , H.div [ HA.class "message-body" ] [ H.text msg.body ]
        ]


viewContentPane : Page -> MyInfo -> Dict String Conversation -> Html Msg
viewContentPane page myInfo convs =
    case page of
        EmptyPage ->
            H.div [ HA.id "content-pane" ] []

        ConversationPage jid ->
            let
                conv =
                    convs |> Dict.get (Jid.toString jid) |> Maybe.withDefault (Conversation.empty jid)
            in
            H.div [ HA.id "content-pane" ]
                [ viewConversation conv
                , conversationsInput conv
                ]


conversationsInput : Conversation -> Html Msg
conversationsInput conv =
    H.div [ HA.id "conversation-input" ]
        [ H.textarea
            [ HA.id "conversation-input-textbox"
            , HE.onInput SetConversationInput
            , onEnter (SendMessage conv.jid conv.textInput)
            , HA.value conv.textInput
            ]
            []
        , H.button
            [ HA.id "conversation-input-send"
            , HE.onClick (SendMessage conv.jid conv.textInput)
            ]
            [ H.text "Send" ]
        ]


viewConversation : Conversation -> Html Msg
viewConversation conv =
    H.div [ HA.id "conversation-view" ] (List.map viewMessage conv.messages)


viewMyInfo : MyInfo -> Html Msg
viewMyInfo myInfo =
    H.div [ HA.id "my-info" ]
        [ H.div [ HA.id "my-jid" ] [ H.text (myInfo.jid |> Jid.bare |> Jid.toString) ] ]


viewConversations : Dict String Conversation -> Html Msg
viewConversations convs =
    H.div [ HA.id "conversations" ]
        (convs
            |> Dict.values
            |> List.map
                (\c ->
                    H.div [ HA.class "conversations-entry", HE.onClick (ConversationPage c.jid |> SetPage) ]
                        [ H.text (Jid.toString c.jid) ]
                )
        )


viewContacts : List Contact -> Html Msg
viewContacts contacts =
    H.div [ HA.id "contacts" ]
        (contacts |> List.map viewContact)


viewContact : Contact -> Html Msg
viewContact contact =
    H.div [ HA.class "contacts-entry", HE.onClick (ConversationPage contact.jid |> SetPage) ]
        [ viewContactAvatar contact
        , H.div [ HA.class "contacts-entry-display" ] [ H.text contact.display ]
        , viewContactStatusCircle contact.presence.show
        ]


viewContactAvatar : Contact -> Html Msg
viewContactAvatar contact =
    case contact.avatar of
        Just avatarData ->
            H.img [ HA.class "contacts-entry-avatar", HA.src avatarData ] []

        Nothing ->
            H.div [ HA.class "contacts-entry-avatar" ] []


viewContactStatusCircle : Presence.Show -> Html Msg
viewContactStatusCircle pres =
    let
        color =
            case pres of
                Presence.Available ->
                    Palette.Tango.chameleon1

                Presence.Chat ->
                    Palette.Tango.chameleon1

                Presence.Away ->
                    Palette.Tango.butter1

                Presence.ExtendedAway ->
                    Palette.Tango.skyBlue1

                Presence.Busy ->
                    Palette.Tango.scarletRed1

                Presence.Unavailable ->
                    Palette.Tango.aluminum2
    in
    H.div [ HA.class "contacts-entry-presence", HA.style "background-color" (Color.toHexString color) ] []


viewRosterPane : MyInfo -> RosterTab -> Dict String Conversation -> List Contact -> Html Msg
viewRosterPane myInfo tab conversations contacts =
    let
        tabView =
            case tab of
                ContactsTab ->
                    viewContacts contacts

                ConversationsTab ->
                    viewConversations conversations

        tabSwitcherButton id txt t =
            H.button [ HA.id id, HA.class "roster-tab-switcher-tab", HE.onClick (SetRosterTab t) ] [ H.text txt ]

        tabSwitcher =
            H.div [ HA.id "roster-tab-switcher" ]
                [ tabSwitcherButton "roster-tab-switcher-contacts" "Contacts" ContactsTab
                , tabSwitcherButton "roster-tab-switcher-conversations" "Conversations" ConversationsTab
                ]
    in
    H.div [ HA.id "roster-pane" ]
        [ viewMyInfo myInfo
        , tabSwitcher
        , tabView
        ]


view : Model -> Html Msg
view model =
    case model.myInfo of
        Just myInfo ->
            H.div [ HA.id "root" ]
                [ viewRosterPane myInfo model.rosterTab model.conversations model.contacts
                , viewContentPane model.page myInfo model.conversations
                ]

        Nothing ->
            H.div [ HA.id "root" ] []
