module Jid exposing
    ( Jid
    , bare
    , parse
    , toString
    )


type alias Jid =
    { node : Maybe String
    , domain : String
    , resource : Maybe String
    }


bare : Jid -> Jid
bare { node, domain } =
    { node = node, domain = domain, resource = Nothing }


toString : Jid -> String
toString { node, domain, resource } =
    let
        nodePart =
            case node of
                Just n ->
                    n ++ "@"

                Nothing ->
                    ""

        resourcePart =
            case resource of
                Just r ->
                    "/" ++ r

                Nothing ->
                    ""
    in
    nodePart ++ domain ++ resourcePart


parse : String -> Jid
parse str =
    let
        ( node, rest ) =
            case String.split "@" str of
                n :: r :: _ ->
                    ( Just n, r )

                [ r ] ->
                    ( Nothing, r )

                [] ->
                    ( Nothing, "" )

        ( domain, resource ) =
            case String.split "/" rest of
                d :: r :: _ ->
                    ( d, Just r )

                [ d ] ->
                    ( d, Nothing )

                [] ->
                    ( "", Nothing )
    in
    { node = node, domain = domain, resource = resource }
