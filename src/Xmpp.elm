port module Xmpp exposing
    ( Cmd(..)
    , Msg(..)
    , cmd
    , sub
    )

import Contact exposing (Contact)
import Jid exposing (Jid)
import Json.Decode as JD
import Json.Encode as JE
import Presence exposing (Presence)


type Msg
    = Connected Jid
    | MessageReceived Jid String
    | ContactPresence Jid Presence
    | Roster (List Contact)
    | RosterAdd Jid
    | RosterRemove Jid
    | SetAvatar Jid String


type Cmd
    = Connect String Jid String
    | SendMessage Jid String


port xmppMsg : (JE.Value -> msg) -> Sub msg


port xmppCmd : JE.Value -> Cmd.Cmd msg


sub : (Result JD.Error Msg -> msg) -> Sub msg
sub f =
    xmppMsg (f << decodeMsg)


cmd : Cmd -> Cmd.Cmd msg
cmd xc =
    xmppCmd (encodeCmd xc)


decodeMsg : JE.Value -> Result JD.Error Msg
decodeMsg val =
    JD.decodeValue msgDecoder val


msgDecoder : JD.Decoder Msg
msgDecoder =
    JD.field "tag" JD.string |> JD.andThen msgDecoderInner


msgDecoderInner : String -> JD.Decoder Msg
msgDecoderInner tag =
    case tag of
        "Connected" ->
            JD.map Connected (JD.field "jid" jidDecoder)

        "MessageReceived" ->
            JD.map2 MessageReceived
                (JD.field "from" jidDecoder)
                (JD.field "body" JD.string)

        "Roster" ->
            JD.map Roster
                (JD.field "items" (JD.list decodeRosterItem))

        "SetAvatar" ->
            JD.map2 SetAvatar
                (JD.field "from" parseJidDecoder)
                (JD.field "data" JD.string)

        "Presence" ->
            JD.map3 (\jid show status -> ContactPresence jid { show = show, status = status })
                (JD.field "jid" jidDecoder)
                (JD.field "show" showDecoder)
                (JD.field "status" (JD.maybe JD.string))

        _ ->
            JD.fail "unimplemented msg"


decodeRosterItem : JD.Decoder Contact
decodeRosterItem =
    JD.map Contact.empty
        (JD.field "jid" jidDecoder)


jidDecoder : JD.Decoder Jid
jidDecoder =
    JD.map3
        (\local domain resource -> { node = local, domain = domain, resource = resource })
        (JD.field "local" (JD.maybe JD.string))
        (JD.field "domain" JD.string)
        (JD.field "resource" (JD.maybe JD.string))


parseJidDecoder : JD.Decoder Jid
parseJidDecoder =
    JD.map Jid.parse JD.string


showDecoder : JD.Decoder Presence.Show
showDecoder =
    JD.string |> JD.map Presence.parseShow


encodeCmd : Cmd -> JE.Value
encodeCmd c =
    case c of
        Connect wsUrl jid pw ->
            JE.object
                [ ( "tag", JE.string "Connect" )
                , ( "jid", JE.string (Jid.toString jid) )
                , ( "password", JE.string pw )
                , ( "wsUrl", JE.string wsUrl )
                ]

        SendMessage jid body ->
            JE.object
                [ ( "tag", JE.string "SendMessage" )
                , ( "to", JE.string (Jid.toString jid) )
                , ( "body", JE.string body )
                ]
