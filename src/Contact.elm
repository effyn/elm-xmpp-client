module Contact exposing (Contact, empty, setAvatar, setPresence)

import Jid exposing (Jid)
import Presence exposing (Presence)


type alias Contact =
    { display : String
    , jid : Jid
    , avatar : Maybe String
    , presence : Presence
    }


empty : Jid -> Contact
empty jid =
    { display = Maybe.withDefault jid.domain jid.node
    , jid = jid
    , avatar = Nothing
    , presence = Presence.unavailable
    }


setAvatar : String -> Contact -> Contact
setAvatar data contact =
    { contact | avatar = Just data }


setPresence : Presence -> Contact -> Contact
setPresence pres contact =
    { contact | presence = pres }
